# Community_Inclusion_Currencies
Repository for Complex Systems model of the Grassroots Economics Community Inclusion Currency project. The Colab notebooks are able to be run and played with by anyone who uses the link. Modeling is built in [cadCAD](https://cadcad.org/). 

## What is cadCAD?
cadCAD (complex adaptive dynamics Computer-Aided Design) is a python based modeling framework for research, validation, and Computer Aided Design of complex systems. Given a model of a complex system, cadCAD can simulate the impact that a set of actions might have on it. This helps users make informed, rigorously tested decisions on how best to modify or interact with the system in order to achieve their goals. cadCAD supports different system modeling approaches and can be easily integrated with common empirical data science workflows. Monte Carlo methods, A/B testing and parameter sweeping features are natively supported and optimized for.

## Simulations

### Subpopulation initialization 
[Click here](SubpopulationGenerator/Subpopulation_Construction.ipynb)

### Simulation work
[Click here](Simulation/CIC_Network_cadCAD_model.ipynb)

#### Parameter sweep 
[Click here](Simulation_param/CIC_Network_cadCAD_model_params.ipynb)

### Theory work
[Click here](Theory/cic_initialization.ipynb)


### Colab - last refresh and syncrhonisation date: 5-20-20
[Click here to get to an interactive notebook](https://colab.research.google.com/github/BlockScience/Community_Inclusion_Currencies/blob/master/Colab/CIC_Network_cadCAD_model.ipynb#scrollTo=UZEO2sP9Mhi-)

[Click here to for an interactive notebook with a parameter sweep](https://colab.research.google.com/github/BlockScience/Community_Inclusion_Currencies/blob/master/Colab/CIC_Network_cadCAD_model_params.ipynb)


## Concepts addressed

####  Systems Thinking
* https://community.cadcad.org/t/introduction-to-systems-thinking/18
* https://community.cadcad.org/t/working-glossary-of-systems-concepts/17

#### cadCAD
* https://community.cadcad.org/t/introduction-to-cadcad/15
* https://community.cadcad.org/t/putting-cadcad-in-context/19
* https://github.com/BlockScience/cadCAD/tree/master/tutorials

#### Token Engineering
* https://blog.oceanprotocol.com/towards-a-practice-of-token-engineering-b02feeeff7ca
* https://assets.pubpub.org/sy02t720/31581340240758.pdf

#### Community Currencies

#### Subpopulation modeling

#### Complex systems
* https://www.frontiersin.org/articles/10.3389/fams.2015.00007/full
* https://epub.wu.ac.at/7433/1/zargham_paruch_shorish.pdf

#### Network theory

#### Economics
* https://ergodicityeconomics.com/lecture-notes/

#### Systems Engineering
* http://systems.hitchins.net/systems-engineering/se-monographs/seessence.pdf


